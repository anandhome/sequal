const db = require('./db');
const model = require('./model');
const {
    User,
} = model;

async function init() {
    await db.authenticate();
    console.log('Connection has been established successfully.');

    // force: true will drop the table if it already exists
    await User.sync({ force: true });
    await User.create({    // Table created
        firstName: 'John',
        lastName: 'Hancock'
    });

    console.log(await User.findAll());
}

init().then(_ => console.log('database initialized successfully.')).catch(console.error);

module.exports = {
    db,
    model,
};
