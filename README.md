# SEQUAL

This is a basic project to show how to write modes in separate files and use an orm sequelize to connect the dots.

This project is based on the standard Sequelize documentation on : `https://sequelize.org/v4/manual/installation/getting-started.html`

## How to setup project?

- Run `git clone git@bitbucket.org:anandhome/sequal.git` from intended project folder.
- If you are using node version above 8, use your `nvm` to change node version to 6 or 8, like `nvm use 8.17.0`
- Run `npm install`

After follwing commands , your project is ready to run.

## How to run the project?

Run usual `node .` or `node index.js` command. This will start the code, for will also create a sqlite DB file in project root.

For any queries , write drop me a mail at : anand.abhishek73@gmail.com
